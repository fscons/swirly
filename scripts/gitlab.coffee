# Description:
#   Post gitlab related events using gitlab hooks
#
# Dependencies:
#   "url" : ""
#   "querystring" : ""
#
# Configuration:
#   GITLAB_CHANNEL
#   GITLAB_DEBUG
#
#   Put http://<HUBOT_URL>:<PORT>/gitlab/system as your system hook
#   Put http://<HUBOT_URL>:<PORT>/gitlab/web as your web hook (per repository)
#   You can also append "?targets=%23room1,%23room2" to the URL to control the
#   message destination.  Using the "target" parameter to override the
#   GITLAB_CHANNEL configuration value.
#
# Commands:
#   None
#
# URLS:
#   /gitlab/system
#   /gitlab/web
#
# Author:
#   omribahumi

url = require 'url'
querystring = require 'querystring'

module.exports = (robot) ->
  gitlabChannel = process.env.GITLAB_CHANNEL or "#gitlab"
  debug = process.env.GITLAB_DEBUG?

  if robot.adapter.constructor.name is 'IrcBot'
    bold = (text) ->
      "\x02" + text + "\x02"
    underline = (text) ->
      "\x1f" + text + "\x1f"
  else
    bold = (text) ->
      text
    underline = (text) ->
      text

  trim_commit_url = (url) ->
    url.replace(/(\/[0-9a-f]{9})[0-9a-f]+$/, '$1')

  handler = (type, req, res) ->
    query = querystring.parse(url.parse(req.url).query)
    hook = req.body

    if debug
      console.log('query', query)
      console.log('hook', hook)

    user = {}
    user.room = if query.targets then query.targets else gitlabChannel
    user.type = query.type if query.type

    switch type
      when "system"
        switch hook.event_name
          when "project_create"
            robot.send user, "Yay! New gitlab project #{bold(hook.name)} created by #{bold(hook.owner_name)} (#{bold(hook.owner_email)})"
          when "project_destroy"
            robot.send user, "Oh no! #{bold(hook.owner_name)} (#{bold(hook.owner_email)}) deleted the #{bold(hook.name)} project"
          when "user_add_to_team"
            robot.send user, "#{bold(hook.project_access)} access granted to #{bold(hook.user_name)} (#{bold(hook.user_email)}) on #{bold(hook.project_name)} project"
          when "user_remove_from_team"
            robot.send user, "#{bold(hook.project_access)} access revoked from #{bold(hook.user_name)} (#{bold(hook.user_email)}) on #{bold(hook.project_name)} project"
          when "user_create"
            robot.send user, "Please welcome #{bold(hook.name)} (#{bold(hook.email)}) to Gitlab!"
          when "user_destroy"
            robot.send user, "We will be missing #{bold(hook.name)} (#{bold(hook.email)}) on Gitlab"
      when "web"
        message = ""
        # is it code being pushed?
        if hook.ref
          branch = hook.ref.split("/")[2..].join("/")
          # if the ref before the commit is 00000, this is a new branch
          if /^0+$/.test(hook.before)
            robot.send user, "#{bold(hook.user_name)} pushed new branch #{bold(branch)} at #{bold(hook.repository.name)}"
            hook.commits.map((commit) ->
                robot.send user, '➦ ' + commit.id.substr(0,8) + ' ' + commit.message.split('\n')[0];
            )
            robot.send user, "#{underline(hook.repository.homepage + '/commits/' + branch)}"
          else
            robot.send user, "#{bold(hook.user_name)} pushed to branch #{branch} at #{hook.repository.name} "
            hook.commits.map((commit) ->
                robot.send user, '➦ ' + commit.id.substr(0,8) + ' ' + commit.message.split('\n')[0];
            )
            robot.send user, "#{underline(hook.repository.homepage + '/compare/' + hook.before.substr(0,9) + '...' + hook.after.substr(0,9))}"
        # not code? must be a something good!
        else
          switch hook.object_kind
            when "issue"
              robot.send user, "Issue #{bold(hook.object_attributes.iid)}: #{hook.object_attributes.title} (#{hook.object_attributes.state}) at #{hook.object_attributes.url}"
            when "merge_request"
              console.log hook.object_attributes.action
              switch hook.object_attributes.action
                when "open"
                  robot.send user, "#{hook.user.name} opened merge request ##{hook.object_attributes.iid} at #{hook.object_attributes.target.namespace}/#{hook.object_attributes.target.name}"
                  robot.send user, '➦ ' + "#{hook.object_attributes.title}"
                  robot.send user, "#{hook.object_attributes.url}"
                when "merge"
                  robot.send user, "#{hook.user.name} accepted merge request ##{hook.object_attributes.iid} at #{hook.object_attributes.target.namespace}/#{hook.object_attributes.target.name}"
                  robot.send user, '➦ ' + "#{hook.object_attributes.title}"
                  robot.send user, "#{hook.object_attributes.url}"
                when "close"
                  robot.send user, "#{hook.user.name} closed merge request ##{hook.object_attributes.iid} at #{hook.object_attributes.target.namespace}/#{hook.object_attributes.target.name}"
                  robot.send user, '➦ ' + "#{hook.object_attributes.title}"
                  robot.send user, "#{hook.object_attributes.url}"
          if hook.object_attributes.description
            robot.send user, ">> #{hook.object_attributes.description}"

  robot.router.post "/gitlab/system", (req, res) ->
    handler "system", req, res
    res.end ""

  robot.router.post "/gitlab/web", (req, res) ->
    handler "web", req, res
    res.end ""

