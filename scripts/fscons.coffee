# Description:
#   Hubot scripts for fscons
#
# Commands:
#   hubot countdown - Time until FSCONS
#

countdown = require('countdown')

module.exports = (robot) ->

  robot.respond /countdown/i, (msg) ->
    s = countdown( new Date(2016, 11, 12,10) ).toString();
    msg.send("#{s} until FSCONS!");

  robot.respond /(hi|hello|hey|hej).*/i, (msg) ->
    console.log(msg.user)
    msg.send("Hello #{msg.message.user.id}, how may I help you?");

  robot.hear /(hi|hello|hey|hej) swirly.*/i, (msg) ->
    msg.send("Hello #{msg.message.user.id}, how may I help you?");
